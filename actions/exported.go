package actions

import (
	// local
	"gitlab.com/pztrn/magister/actions/admin"
	"gitlab.com/pztrn/magister/actions/users"
	"gitlab.com/pztrn/magister/internal/http"
)

func Initialize() {
	admin.Initialize()
	users.Initialize()

	http.SetDefaultHandlerForErrors(NotFoundGET)

	http.E.GET("/", indexGET)
}
